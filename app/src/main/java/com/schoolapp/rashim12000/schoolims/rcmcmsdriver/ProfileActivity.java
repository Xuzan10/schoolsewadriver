package com.schoolapp.rashim12000.schoolims.rcmcmsdriver;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.zip.Inflater;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {
    private TextView username, email, phonenum, text;
    private CircleImageView driverimage;
    private Button goonline, logout;
    private FirebaseAuth auth;
    private FirebaseStorage mStorage;
    private FirebaseUser user;
    private StorageReference mStorageReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        String emailval = user.getEmail();


        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();


        }


        mStorageReference = FirebaseStorage.getInstance().getReference();

        username = findViewById(R.id.username);
        email = findViewById(R.id.email);

        phonenum = findViewById(R.id.phonenum);
        driverimage = findViewById(R.id.driverimage);
        goonline = findViewById(R.id.goonline);
        logout = findViewById(R.id.logout);
        text = findViewById(R.id.text);
        email.setText(emailval);
        String encodeemail = EditProfileActivity.EncodeString(user.getEmail());
        String path = "Drivers/" + encodeemail;
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference(path);
        text.setVisibility(View.GONE);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String usernamevalue = dataSnapshot.child("username").getValue().toString();
                String phonenumbervalue = dataSnapshot.child("phonenumber").getValue().toString();
                username.setText(usernamevalue);
                phonenum.setText(phonenumbervalue);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        String path2 = "DriverPhotos";
        mStorageReference.child(path2).child(user.getEmail()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(ProfileActivity.this).load(uri).into(driverimage);

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
            }
        });

        goonline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileActivity.this, TrackerActivity.class));
                goonline.setVisibility(View.GONE);
                logout.setVisibility(View.GONE);
                text.setVisibility(View.VISIBLE);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                auth.signOut();
                startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
                finish();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.edit){
            Intent intent = new Intent(ProfileActivity.this,EditProfileActivity.class);
            intent.putExtra("id",1);
            intent.putExtra("email2",email.getText().toString());
            intent.putExtra("username",username.getText().toString());
            intent.putExtra("phonenumber",phonenum.getText().toString());
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }


    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });

        final AlertDialog alert = builder.create();
        alert.show();

    }
}
